package com.epam.mo.molistapp.presentation.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.epam.mo.molistapp.R
import com.epam.mo.molistapp.domain.items.Talk

class TalksAdapter(private val itemClickListener: (Talk) -> Unit): RecyclerView.Adapter<TalkViewHolder>() {
    private var items: List<Talk> = emptyList()

    internal fun submitItems(items: List<Talk>) {
        this.items = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TalkViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_talk, parent, false)
        return TalkViewHolder(view, itemClickListener)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: TalkViewHolder, position: Int) = holder.bindTo(items[position])
}