package com.epam.mo.molistapp.dagger

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory
import com.epam.mo.molistapp.presentation.detail.DetailFragment
import com.epam.mo.molistapp.presentation.home.HomeFragment
import dagger.Binds
import dagger.Module
import dagger.android.support.FragmentKey
import dagger.multibindings.IntoMap

@Module
abstract class FragmentModule {

    @Binds
    abstract fun bindFragmentFactory(factory: FragmentInjectionFactory): FragmentFactory

    @Binds
    @IntoMap
    @FragmentKey(HomeFragment::class)
    abstract fun bindHomeFragment(fragment: HomeFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(DetailFragment::class)
    abstract fun bindDetailFragment(fragment: DetailFragment): Fragment
}
