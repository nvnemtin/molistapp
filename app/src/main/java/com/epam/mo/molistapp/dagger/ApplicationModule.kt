package com.epam.mo.molistapp.dagger

import android.content.Context
import com.epam.mo.molistapp.MoListApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [
    HomeModule::class
])
class ApplicationModule(private val application: MoListApplication) {
    @Singleton
    @Provides
    internal fun provideContext(): Context {
        return application.applicationContext
    }
}
