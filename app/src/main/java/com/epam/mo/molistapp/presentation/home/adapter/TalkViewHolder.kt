package com.epam.mo.molistapp.presentation.home.adapter

import android.net.Uri
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.epam.mo.molistapp.R
import com.epam.mo.molistapp.domain.items.Talk
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_talk.*

class TalkViewHolder (
    override val containerView: View,
    private val itemClickListener: (Talk) -> Unit
): RecyclerView.ViewHolder(containerView), LayoutContainer {
    private var item: Talk? = null
    private val radius = containerView.context.resources.getDimensionPixelSize(R.dimen.corner_radius)
    init {
        containerView.setOnClickListener {
            item?.let {
                itemClickListener.invoke(it)
            }
        }
    }

    internal fun bindTo(item: Talk) {
        this.item = item
        first_name.text = item.firstName
        last_name.text = item.lastName
        title.text = item.title
        image.visibility = if (item.imageFilename.isEmpty()) { View.GONE } else { View.VISIBLE }
        first_name.visibility = if (item.firstName.isEmpty()) { View.GONE } else { View.VISIBLE }
        last_name.visibility = if (item.lastName.isEmpty()) { View.GONE } else { View.VISIBLE }
        title.visibility = if (item.title.isEmpty()) { View.GONE } else { View.VISIBLE }
        val nameTextColor = if (adapterPosition % 2 == 0) {
            ContextCompat.getColor(itemView.context, R.color.yellow)
        } else {
            ContextCompat.getColor(itemView.context, R.color.purple)
        }
        first_name.setTextColor(nameTextColor)
        last_name.setTextColor(nameTextColor)
        Glide
            .with(image)
            .load(Uri.parse("file:///android_asset/${item.imageFilename}.jpg"))
            .transform(RoundedCorners(radius))
            .into(image)
    }
}