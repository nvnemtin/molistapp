package com.epam.mo.molistapp.dagger

import com.epam.mo.molistapp.presentation.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector
    internal abstract fun contributeMainActivityInjector(): MainActivity
}
