package com.epam.mo.molistapp.data.home

import android.content.Context
import com.epam.mo.molistapp.data.TalkDto
import com.epam.mo.molistapp.domain.home.TalksDataSource
import com.epam.mo.molistapp.domain.items.Platform
import com.epam.mo.molistapp.domain.items.Talk
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import io.reactivex.Maybe
import io.reactivex.Single
import javax.inject.Inject

private const val JSON_FILE = "talksData.json"
private val allPlatforms = setOf(
    Platform.ANDROID,
    Platform.IOS,
    Platform.MAIN_TRACK,
    Platform.BOTH
)

class DefaultTalksDataSource @Inject constructor(
    private val context: Context,
    private val moshi: Moshi,
    private val mapper: (@JvmSuppressWildcards TalkDto) -> @JvmSuppressWildcards Talk
) : TalksDataSource {
    private val jsonAdapter by lazy {
        val type = Types.newParameterizedType(List::class.java, TalkDto::class.java)
        moshi.adapter<List<TalkDto>>(type)
    }

    override fun getTalks(platforms: Set<Platform>): Single<List<Talk>> = Single
        .fromCallable {
            val raw = context.assets.open(JSON_FILE).bufferedReader().use {
                it.readText()
            }
            jsonAdapter.fromJson(raw)
        }
        .map {
            it.map(mapper)
        }
        .map { list ->
            list.filter { platforms.contains(it.platform) }
        }

    override fun getTalk(talkId: Int): Maybe<Talk> =
        getTalks(allPlatforms)
            .flattenAsObservable {
                it
            }
            .filter {
                it.id == talkId
            }
            .firstElement()
}