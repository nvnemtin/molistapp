package com.epam.mo.molistapp.dagger

import com.epam.mo.molistapp.data.TalkDto
import com.epam.mo.molistapp.data.home.DefaultTalksDataSource
import com.epam.mo.molistapp.data.home.TalksDtoMapper
import com.epam.mo.molistapp.domain.home.TalksDataSource
import com.epam.mo.molistapp.domain.items.Talk
import com.squareup.moshi.Moshi
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [
    HomeDataMappersModule::class
])
interface HomeModule {
    @Binds
    @Singleton
    fun bindTalksDataSource(impl: DefaultTalksDataSource): TalksDataSource
}

@Module
class HomeDataMappersModule {
    @Provides
    fun provideTalkDtoMapper(): (TalkDto) -> Talk = TalksDtoMapper()::map

    @Provides
    @Singleton
    fun provideMoshi(): Moshi = Moshi.Builder().build()
}