package com.epam.mo.molistapp.presentation.detail

import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.epam.mo.molistapp.R
import com.epam.mo.molistapp.domain.items.Talk
import com.epam.mo.molistapp.presentation.common.loadCircularImage
import kotlinx.android.synthetic.main.fragment_detail.*
import kotlinx.android.synthetic.main.fragment_detail.image
import kotlinx.android.synthetic.main.item_talk.*
import javax.inject.Inject

class DetailFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory
) : Fragment() {
    private val viewModel by viewModels<DetailViewModel> { viewModelFactory }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_detail, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupFragment()
        bindViewModel()
        if (savedInstanceState == null) {
            arguments?.let {
                viewModel.loadTalkData(it.getInt(Talk.TALK_ID))
            }
        }
    }

    private fun setupFragment() {}

    private fun bindViewModel() {
        val borderWidth = requireContext().resources.getDimensionPixelSize(R.dimen.border_width).toFloat()
        viewModel.viewState.observe(viewLifecycleOwner, Observer {
            when (it) {
                DetailViewModel.ViewState.Loading -> {
                }
                is DetailViewModel.ViewState.Content -> {
                    val nameTextColor = if (it.talk.id % 2 != 0) {
                        ContextCompat.getColor(requireContext(), R.color.yellow)
                    } else {
                        ContextCompat.getColor(requireContext(), R.color.purple)
                    }
                    image.loadCircularImage(
                        model = Uri.parse("file:///android_asset/${it.talk.imageFilename}.jpg"),
                        borderColor = Color.WHITE,
                        borderSize = borderWidth
                    )
                    speaker.text = "${it.talk.firstName} ${it.talk.lastName}"
                    speaker.setTextColor(nameTextColor)
                    country.text = it.talk.country
                    role.text = it.talk.shortDescription
                    description.text = it.talk.description
                }
                DetailViewModel.ViewState.Error -> Toast.makeText(
                    requireContext(),
                    "Something went wrong",
                    Toast.LENGTH_LONG
                ).show()
            }
        })
    }
}