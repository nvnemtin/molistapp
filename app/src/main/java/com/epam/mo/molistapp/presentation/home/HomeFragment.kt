package com.epam.mo.molistapp.presentation.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.epam.mo.molistapp.R
import com.epam.mo.molistapp.domain.items.Talk
import com.epam.mo.molistapp.presentation.home.adapter.TalksAdapter
import kotlinx.android.synthetic.main.fragment_home.*
import javax.inject.Inject

class HomeFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory
) : Fragment() {
    private val viewModel by activityViewModels<HomeViewModel> { viewModelFactory }
    private lateinit var adapter: TalksAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupFragment()
        bindViewModel()
        viewModel.refresh(forced = false)
    }

    private fun setupFragment() {
        adapter = TalksAdapter {
            findNavController().navigate(R.id.action_home_to_detail, bundleOf(Talk.TALK_ID to it.id))
        }
        swipe_refresh.setOnRefreshListener {
            viewModel.refresh(forced = true)
        }
        recycler_view.layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        recycler_view.adapter = adapter
    }

    private fun bindViewModel() {
        viewModel.viewState.observe(viewLifecycleOwner, Observer {
            when(it) {
                HomeViewModel.ViewState.Loading -> {
                    swipe_refresh.isRefreshing = true
                    swipe_refresh.visibility = View.VISIBLE
                    retry.visibility = View.GONE
                }
                is HomeViewModel.ViewState.Content -> {
                    swipe_refresh.isRefreshing = false
                    swipe_refresh.visibility = View.VISIBLE
                    retry.visibility = View.GONE
                    adapter.submitItems(it.items)
                }
                HomeViewModel.ViewState.Error -> {
                    swipe_refresh.isRefreshing = false
                    swipe_refresh.visibility = View.GONE
                    retry.visibility = View.VISIBLE
                }
            }
        })
    }
}
