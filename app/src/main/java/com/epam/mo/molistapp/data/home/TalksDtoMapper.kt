package com.epam.mo.molistapp.data.home

import com.epam.mo.molistapp.data.TalkDto
import com.epam.mo.molistapp.domain.items.Platform
import com.epam.mo.molistapp.domain.items.Talk

class TalksDtoMapper {
    fun map(jsonDto: TalkDto): Talk =
        Talk(
            id = jsonDto.id ?: 0,
            imageFilename = jsonDto.imageFilename ?: "",
            platform = Platform.fromString(jsonDto.platform),
            firstName = (jsonDto.firstName ?: "").toUpperCase(),
            lastName = (jsonDto.lastName ?: "").toUpperCase(),
            title = jsonDto.title ?: "",
            description = jsonDto.description ?: "",
            shortDescription = jsonDto.shortDescription ?: "",
            speakerDescription = jsonDto.speakerDescription ?: "",
            country = jsonDto.country ?: "",
            time = jsonDto.time ?: "",
            links = jsonDto.links ?: emptyList()
        )
}