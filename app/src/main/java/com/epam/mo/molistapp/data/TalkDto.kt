package com.epam.mo.molistapp.data

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class TalkDto(
    @Json(name = "id")
    val id: Int?,
    @Json(name = "imagePath")
    val imageFilename: String?,
    @Json(name = "platform")
    val platform: String?,
    @Json(name = "firstName")
    val firstName: String?,
    @Json(name = "lastName")
    val lastName: String?,
    @Json(name = "talkTitle")
    val title: String?,
    @Json(name = "talkDescription")
    val description: String?,
    @Json(name = "shortDescription")
    val shortDescription: String?,
    @Json(name = "speakersDescription")
    val speakerDescription: String?,
    @Json(name = "country")
    val country: String?,
    @Json(name = "time")
    val time: String?,
    @Json(name = "linksToSocial")
    val links: List<Map<String, String>>?
)

/*
"id":9,
        "imagePath":"photo_2018-07-04_21-",
        "platform": "Android",
        "firstName": "KONSTANTIN",
        "lastName": "TSKHOVREBOV",
        "talkTitle": "Android Insets — deal with fears and get ready for Android Q (RUS)",
        "talkDescription": "With the release of Android Q, two important things must be done: to support the system navigation with gestures and to make the status and navigation bars transparent. To do this, you need to stop looking for workarounds and figure out how insets work!\nI will talk about a long history of avoiding this problem, about studying and fighting with android, and finally about a complete understanding of this topic.\nIn addition to supporting new recommendations in the system, we will stop being afraid of the keyboard, learn how to recognize its size and respond to its appearance.",
        "shortDescription": "Android Lead @Redmadrobot",
        "speakersDescription": "I am a graduate of SPBSTU.\nI used to be fond of microcontrollers and circuitry.\nI started coding for android when the first popular version of 2.2 appeared. At the moment I am an architect in the St. Petersburg team RedMadRobot. I am looking for simple solutions for complex tasks, and I like to discuss this with single minded people.",
        "linksToSocial": [
            {"twitter": "http://twitter.com/terrakok"},
            {"link": "http://angrybyte.me/"},
            {"telegram": "https://t.me/neiskluchenie"},
            {"instagram": "http://twitter.com/colriot"}
        ],
        "country": "Russia",
        "time": "11:50-12:30"
 */