package com.epam.mo.molistapp.presentation.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.epam.mo.molistapp.domain.home.TalksDataSource
import com.epam.mo.molistapp.domain.items.Platform
import com.epam.mo.molistapp.domain.items.Talk
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class HomeViewModel @Inject constructor(
    private val dataSource: TalksDataSource
) : ViewModel() {
    private val compositeDisposable = CompositeDisposable()
    private val _viewState = MutableLiveData<ViewState>().apply { value = ViewState.Loading }
    internal val viewState: LiveData<ViewState>
        get() = _viewState

    internal fun refresh(forced: Boolean) {
        if (forced || _viewState.value !is ViewState.Content) {
            _viewState.value = ViewState.Loading
            compositeDisposable.add(
                dataSource
                    .getTalks(platforms = setOf(Platform.ANDROID, Platform.IOS, Platform.BOTH))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeBy(
                        onSuccess = {
                            _viewState.value = ViewState.Content(
                                items = it
                            )
                        },
                        onError = {
                            _viewState.value = ViewState.Error
                        }
                    )
            )
        }
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    internal sealed class ViewState {
        object Loading: ViewState()
        data class Content(
            val items: List<Talk>
        ): ViewState()
        object Error: ViewState()
    }
}
