package com.epam.mo.molistapp.domain.home

import com.epam.mo.molistapp.domain.items.Platform
import com.epam.mo.molistapp.domain.items.Talk
import io.reactivex.Maybe
import io.reactivex.Single

interface TalksDataSource {
    fun getTalks(platforms: Set<Platform>): Single<List<Talk>>
    fun getTalk(talkId: Int): Maybe<Talk>
}