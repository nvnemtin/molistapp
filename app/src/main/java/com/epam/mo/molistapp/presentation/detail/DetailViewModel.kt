package com.epam.mo.molistapp.presentation.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.epam.mo.molistapp.domain.home.TalksDataSource
import com.epam.mo.molistapp.domain.items.Talk
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class DetailViewModel @Inject constructor(
    private val dataSource: TalksDataSource
) : ViewModel() {
    private val _viewState = MutableLiveData<ViewState>().apply { value = ViewState.Loading }
    internal val viewState: LiveData<ViewState>
        get() = _viewState
    private val compositeDisposable = CompositeDisposable()

    internal fun loadTalkData(talkId: Int?) {
        if (talkId == null) {
            _viewState.value = ViewState.Error
            return
        }
        _viewState.value = ViewState.Loading
        compositeDisposable.add(
            dataSource
                .getTalk(talkId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onSuccess = {
                        _viewState.value = ViewState.Content(talk = it)
                    },
                    onError = {
                        _viewState.value = ViewState.Error
                    }
                )
        )
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    internal sealed class ViewState {
        object Loading : ViewState()
        data class Content(
            val talk: Talk
        ) : ViewState()

        object Error : ViewState()
    }
}