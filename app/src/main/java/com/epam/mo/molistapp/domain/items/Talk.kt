package com.epam.mo.molistapp.domain.items


data class Talk(
    val id: Int,
    val imageFilename: String,
    val platform: Platform,
    val firstName: String,
    val lastName: String,
    val title: String,
    val description: String,
    val shortDescription: String,
    val speakerDescription: String,
    val country: String,
    val time: String,
    val links: List<Map<String, String>>
) {
    companion object {
        const val TALK_ID = "talkId"
    }
}

enum class Platform(val constantName: String) {
    ANDROID("Android"),
    IOS("iOS"),
    MAIN_TRACK("Main scene"),
    BOTH("Both");

    companion object {
        fun fromString(raw: String?): Platform =
            when (raw) {
                ANDROID.constantName -> ANDROID
                IOS.constantName -> IOS
                MAIN_TRACK.constantName -> MAIN_TRACK
                BOTH.constantName -> BOTH
                else -> throw IllegalArgumentException("wrong arg $raw for Platform.fromString")
            }

    }
}