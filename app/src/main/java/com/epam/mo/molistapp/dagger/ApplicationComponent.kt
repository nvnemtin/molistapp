package com.epam.mo.molistapp.dagger

import com.epam.mo.molistapp.MoListApplication
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [
    ActivityModule::class,
    ApplicationModule::class,
    FragmentModule::class,
    ViewModelModule::class
])
interface ApplicationComponent{
    fun inject(app: MoListApplication)
}
